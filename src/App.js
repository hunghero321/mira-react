import React, { Component } from "react";
import { debounce } from "lodash";
import Header from "./components/header";
import { Col, Container, Row } from "react-bootstrap";
import windowSize from "react-window-size";
import "./styles/style.scss";
import image1 from "./assets/images/happinessmira.jpg";
import image2 from "./assets/images/promo_img_1-1.jpg";
import image3 from "./assets/images/iphonefirmware-com.jpg";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      scrollPositionY: 0
    };
  }
  componentDidMount() {
    return window.addEventListener("scroll", debounce(this.handleScroll, 16));
  }

  componentWillUnmount() {
    return window.removeEventListener(
      "scroll",
      debounce(this.handleScroll, 16)
    );
  }

  handleScroll = () => {
    const scrollPositionY = +window.scrollY;
    return this.setState({ scrollPositionY });
  };

  render() {
    const { windowWidth } = this.props;
    const isScrolling = !!this.state.scrollPositionY;
    return (
      <div className="App">
        <Header isScrolling={isScrolling} />
        <section className="header-banner">
          <div className="triangle-wrap triangle-wrap--align-right">
            <div className="triangle">
              <div
                className="triangle__img"
                style={{
                  backgroundImage: `url(${image1})`
                }}
              />
              <div className="triangle__gradient" />
            </div>
          </div>
        </section>
        <Container className="section-boxed">
          <Row>
            <Col xs="12" md="6">
              <div className="widget-container">
                <div className="banner-title">
                  <h1 className="heading-title">
                    Find affordable primary care regardless of insurance status.
                  </h1>
                  <p className="heading-description">
                    We search through thousands of data points to help you find
                    the most affordable healthcare appointment personalized to
                    your needs and location. Effortlessly!
                    <img
                      className="emoji"
                      draggable="false"
                      src="https://s.w.org/images/core/emoji/11/svg/1f600.svg"
                      alt="😀"
                    />
                  </p>
                </div>
              </div>
            </Col>
          </Row>
          <Row className="center">
            <Col xs="12" md="9">
              <div className="widget-container">
                <div className="section-center-title">
                  <h2>
                    Accessing affordable healthcare wasn't possible without a
                    health plan
                    <span className="highlight">until now</span>
                  </h2>
                </div>
              </div>
            </Col>
          </Row>
        </Container>
        <Container fluid className="section-boxed section-grid-box">
          <Row>
            <Col xs="12" md="4" className="no-padding">
              <div className="widget-container">
                <div className="populated">
                  <div className="feature feature--box box-shadow hover-up hover-line">
                    <span className="feature__icon" />
                    <h4 className="feature__title">
                      <span>Exclusive upfront pricing</span>
                    </h4>
                    <div className="feature__text elementor-text-editor elementor-clearfix">
                      <p>
                        We work <strong>directly</strong> with your local
                        doctors to get you the best possible cash price
                        comparable for even better than insurance rates without
                        paying 3-4K a year in premiums.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </Col>
            <Col xs="12" md="4" className="no-padding">
              <div className="widget-container">
                <div className="populated">
                  <div className="feature feature--box box-shadow hover-up hover-line">
                    <span className="feature__icon" />
                    <h4 className="feature__title">
                      <span>10X better than online booking</span>
                    </h4>
                    <div className="feature__text elementor-text-editor elementor-clearfix">
                      <p>
                        Forget doctor directory or multiple phone calls to just
                        find an appointment, let our{" "}
                        <strong>artificial intelligent engine</strong> does the
                        work and recommends options that work for you!
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </Col>
            <Col xs="12" md="4" className="no-padding">
              <div className="widget-container">
                <div className="populated">
                  <div className="feature feature--box box-shadow hover-up hover-line">
                    <span className="feature__icon" />
                    <h4 className="feature__title">
                      <span>Zero commitment</span>
                    </h4>
                    <div className="feature__text elementor-text-editor elementor-clearfix">
                      <p>
                        We believe in healthcare to enable life, not vice versa.
                        We want you to have <strong>freedom of choice.</strong>
                        &nbsp;If you want to be a member, that’s cool, or else
                        you can a la carte for services you need.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </Col>
          </Row>
        </Container>
        <section className="background-image-left">
          <div className="triangle-wrap triangle-wrap--align-left">
            <div className="triangle">
              <div
                className="triangle__img"
                style={{
                  backgroundImage: `url(${image2})`
                }}
              />
              <div className="triangle__gradient-left" />
            </div>
          </div>
        </section>
        <Container fluid className="section-boxed">
          <Row className="section-problem-row">
            <Col xs="12" md="12" lg="7">
              <div className="widget-container">
                <div className="section-problem">
                  <h2 className="heading-title">The problem we are solving</h2>
                  <div className="text-editor">
                    <p>
                      Healthcare is expensive because of the many middlemen who
                      each takes a big piece of your hard-earned paychecks.
                      Doctors are increasingly frustrated with the number of
                      mandates and back office work that come with being caught
                      in between.
                    </p>
                    <p>
                      <strong>
                        We are the first platform at scale to enable doctors to
                        work directly with patients regardless of your insurance
                        status
                      </strong>
                      . Because of our simplicity, we’re able to bring the cost
                      down while saving doctor time on extra paperwork.
                    </p>
                  </div>
                </div>
              </div>
            </Col>
          </Row>
        </Container>
        <Container fluid={windowWidth < 992 ? true : false}>
          <Row>
            <Col xs="12" md="6">
              <div className="widget-container">
                <div className="populated">
                  <div className="feature feature--box box-shadow hover-up hover-line">
                    <span className="feature__icon" />
                    <h4 className="feature__title">
                      <span>ONE PLATFORM, INFINITE ACCESS</span>
                    </h4>
                    <div className="feature__text elementor-text-editor elementor-clearfix">
                      <p>
                        Powered by AI, you can access Mira through multiple
                        channels including{" "}
                        <b>
                          web, phone, text, Alexa, Google Assistant, and
                          more.&nbsp;
                        </b>
                        High quality and affordable healthcare is always one
                        step away regardless of where you are.&nbsp;
                      </p>
                      <p>
                        We’re also available as an enterprise solution to
                        automate appointment booking and intake.&nbsp;
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </Col>
            <Col xs="12" md="6">
              <div className="widget-container">
                <div className="widget-image">
                  <img src={image3} alt="" className="image" />
                </div>
              </div>
            </Col>
          </Row>
          <Row className="widget-row-right">
            <Col xs="12" md="6">
              <div className="widget-container">
                <a href="#" className="button-link">
                  <span>I have use case for my company!</span>
                  <i className="fa fa-angle-right" />
                </a>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default windowSize(App);
