import React from "react";
import classnames from "classnames";
import PropTypes from "prop-types";
import { Navbar, Nav, Button, Image } from "react-bootstrap";
import Logo from "../../assets/images/logo/Logomid.png";

const Header = ({ isScrolling }) => (
  <header>
    <Navbar
      collapseOnSelect={false}
      expand="md"
      fixed="top"
      className={classnames(
        "nav__container container-fluid container-semi-fluid nav__holder",
        {
          scrolling: isScrolling
        }
      )}
    >
      <Navbar.Brand href="#home">
        <Image src={Logo} className="logo" />
      </Navbar.Brand>
      <Navbar.Collapse id="responsive-navbar-nav">
        <Nav className="mr-auto">
          <Nav.Link href="https://talktomira.com/about">About</Nav.Link>
          <Nav.Link href="https://talktomira.com/provider">
            For Provider
          </Nav.Link>
          <Nav.Link href="https://talktomira.com/signup">
            Get Early Access
          </Nav.Link>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
    <Button className="customLauncher">Find Care Now</Button>
  </header>
);
Header.propTypes = {
  isScrolling: PropTypes.bool
};
export default Header;
